tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> program(nazwa={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.isValidSymbol($i1.text);} -> podstaw(n={$i1.text},p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       {globals.isValidSymbol($ID.text);} -> id(n={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> if_statement(p1={$e1.st}, p2={$e2.st}, p3={$e3.st}, num={numer.toString()})
        ;
    