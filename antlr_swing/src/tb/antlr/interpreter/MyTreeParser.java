package tb.antlr.interpreter;

import java.util.ArrayList;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	GlobalSymbols globalSymbols = new GlobalSymbols();
	ArrayList<Integer> values = new ArrayList<>();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer div(Integer a, Integer b) {
		return a / b;
	}
	
	protected Integer assign(String varName, Integer value) {
		globalSymbols.setSymbol(varName, value);
		return value;
	}
	
	protected Integer get(String varName) {
		return globalSymbols.getSymbol(varName);
	}

	protected void define(String varName) {
		globalSymbols.newSymbol(varName);
	}
	
	protected void defineHash(Integer value) {
		values.add(value);
	}
	
	protected Integer getHash(Integer i) {
		return values.get(i+1);
	}

}