tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : ( 
          e=expr {drukuj ($e.text + " = " + $e.out.toString()); defineHash($e.out);}
          | ^(VAR i1=ID) {define($i1.text);}
        )* 
        ;

expr returns [Integer out]
        : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = assign($i1.text, $e2.out);}
        | (i1=ID)                  {$out = get($i1.text);}
        | INT                      {$out = getInt($INT.text);}
        | ^(HASH i1=INT)  {$out = getHash(getInt($i1.text));}
        ;
